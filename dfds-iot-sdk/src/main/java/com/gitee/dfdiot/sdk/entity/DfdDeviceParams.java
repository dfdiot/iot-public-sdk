package com.gitee.dfdiot.sdk.entity;

import java.io.Serializable;

/**
 * 设备相关参数
 */
public class DfdDeviceParams implements Serializable {
    private String deviceId;
    private String moduleMark;
    private String cmd;
    private String synck;
    private String data;

    private DfdDeviceParams() {}

    public static DfdDeviceParams instructions(String deviceId, String moduleMark, String cmd, String synck, String data) {
        DfdDeviceParams dfdDeviceParams = new DfdDeviceParams();
        dfdDeviceParams.setDeviceId(deviceId);
        dfdDeviceParams.setModuleMark(moduleMark);
        dfdDeviceParams.setCmd(cmd);
        dfdDeviceParams.setSynck(synck);
        dfdDeviceParams.setData(data);
        return dfdDeviceParams;
    }

    private void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    private void setModuleMark(String moduleMark) {
        this.moduleMark = moduleMark;
    }

    private void setCmd(String cmd) {
        this.cmd = cmd;
    }

    private void setSynck(String synck) {
        this.synck = synck;
    }

    private void setData(String data) {
        this.data = data;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getModuleMark() {
        return moduleMark;
    }

    public String getCmd() {
        return cmd;
    }

    public String getSynck() {
        return synck;
    }

    public String getData() {
        return data;
    }
}
