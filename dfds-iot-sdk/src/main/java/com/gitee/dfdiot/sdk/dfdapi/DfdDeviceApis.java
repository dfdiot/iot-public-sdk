package com.gitee.dfdiot.sdk.dfdapi;

import com.alibaba.fastjson.JSONObject;
import com.gitee.dfdiot.sdk.config.DfdConfig;
import com.gitee.dfdiot.sdk.entity.DfdDeviceParams;
import com.gitee.dfdiot.sdk.service.DfdDeviceService;

/**
 * 设备管理_相关接口
 * yuanjin
 * 2022-10-11
 */
public class DfdDeviceApis {
    private DfdConfig dfdConfig;

    public DfdDeviceApis(DfdConfig dfdConfig) {
        this.dfdConfig = dfdConfig;
    }

    private DfdDeviceService dfdDeviceService = DfdDeviceService.getInstance();

    public JSONObject instructions(DfdDeviceParams deviceParams) {
        return dfdDeviceService.instructions(deviceParams, dfdConfig);
    }
}
