package com.gitee.dfdiot.sdk.utils.common;


import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 参数配置
 *
 * @author yuanjin
 * @since 2022-10-13
 */
public class ParamsUtils {


    public static Map<String, String> getAllParam(Map<String, String> param, Object... object) {
        for (Object o : object) {
            JSONObject object1 = (JSONObject) JSONObject.toJSON(o);
            for (String key : object1.keySet()) {
                if (!("openUrl".equals(key) || "accessKeySecret".equals(key) || "apiVersion".equals(key)) && null != object1.getString(key)) {
                    param.put(key, object1.getString(key));
                }
            }
        }
        return param;
    }

    /**
     * 整合所有参数
     *
     * @param object
     * @return
     */
    public static Map<String, String> getAllParamNoOther(Object... object) {
        Map<String, String> param = new HashMap<>();
        for (Object o : object) {
            JSONObject object1 = (JSONObject) JSONObject.toJSON(o);
            for (String key : object1.keySet()) {
                if (!("openUrl".equals(key) || "accessKeySecret".equals(key) || "apiVersion".equals(key))
                        && object1.containsKey(key) && null != object1.getString(key)) {
                    param.put(key, object1.getString(key));
                }
            }
        }
        return param;
    }

}
