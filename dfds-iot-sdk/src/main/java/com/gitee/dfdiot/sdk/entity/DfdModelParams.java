package com.gitee.dfdiot.sdk.entity;

public class DfdModelParams {
    private String deviceId;

    private DfdModelParams() {
    }

    public static DfdModelParams getModelInfo(String deviceId) {
        DfdModelParams dfdModelParams = new DfdModelParams();
        dfdModelParams.setDeviceId(deviceId);
        return dfdModelParams;
    }

    private void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }
}
