package com.gitee.dfdiot.sdk.service;

import com.alibaba.fastjson.JSONObject;
import com.gitee.dfdiot.sdk.config.DfdConfig;
import com.gitee.dfdiot.sdk.entity.DfdDeviceParams;
import com.gitee.dfdiot.sdk.utils.common.ParamsUtils;
import com.gitee.dfdiot.sdk.utils.common.ResultUtils;
import com.gitee.dfdiot.sdk.utils.request.HttpClientUtil;
import com.gitee.dfdiot.sdk.utils.signs.SignUtil;

import java.util.Map;

/**
 * 设备相关
 * yuanjin
 * 2022-10-14
 */
public class DfdDeviceService {
    private final String device = "/device";
    private HttpClientUtil instance = HttpClientUtil.getInstance();
    private static DfdDeviceService dfdDeviceService = null;

    private DfdDeviceService() {
    }

    public static DfdDeviceService getInstance() {
        if (dfdDeviceService == null) {
            dfdDeviceService = new DfdDeviceService();
        }
        return dfdDeviceService;
    }

    /**
     * 下发设备指令
     * params {
     * "cmd": get（下发指令操作类型）
     * "synch":true
     * "data":"[\"functionMark_1\",\"functionMark_2\"]"
     * }
     */
    public JSONObject instructions(DfdDeviceParams deviceParams, DfdConfig dfdConfig) {
        try {
            Map<String, String> getParams = SignUtil.generate("POST", ParamsUtils.getAllParamNoOther(dfdConfig, deviceParams), dfdConfig.getAccessSecret());
            String response = instance.doPost(dfdConfig.getOpenUrl() + dfdConfig.getApiVersion()  + device + "/instructions", getParams);
            return ResultUtils.returnJson(response);
        } catch (Exception e) {
        }
        return ResultUtils.returnJson(null);
    }

}

