package com.gitee.dfdiot.sdk.service;

import com.alibaba.fastjson.JSONObject;
import com.gitee.dfdiot.sdk.config.DfdConfig;
import com.gitee.dfdiot.sdk.entity.DfdDeviceParams;
import com.gitee.dfdiot.sdk.entity.DfdModelParams;
import com.gitee.dfdiot.sdk.utils.common.ParamsUtils;
import com.gitee.dfdiot.sdk.utils.common.ResultUtils;
import com.gitee.dfdiot.sdk.utils.request.HttpClientUtil;
import com.gitee.dfdiot.sdk.utils.signs.SignUtil;

import java.util.Map;

/**
 * 模型相关
 * yuanjin
 * 2022-10-14
 */
public class DfdModelService {
    private final String model = "/model";
    private HttpClientUtil instance = HttpClientUtil.getInstance();
    private static DfdModelService dfdModelService = null;

    private DfdModelService() {
    }

    public static DfdModelService getInstance() {
        if (dfdModelService == null) {
            dfdModelService = new DfdModelService();
        }
        return dfdModelService;
    }

    /**
     * 查询模型详细信息
     */
    public JSONObject getModelInfo(DfdModelParams modelParams, DfdConfig dfdConfig) {
        try {
            Map<String, String> getParams = SignUtil.generate("GET", ParamsUtils.getAllParamNoOther(dfdConfig, modelParams), dfdConfig.getAccessSecret());
            String response = instance.doPost(dfdConfig.getOpenUrl() + dfdConfig.getApiVersion() + model + "/getModelInfo", getParams);
            return ResultUtils.returnJson(response);
        } catch (Exception e) {
        }
        return ResultUtils.returnJson(null);
    }

}

