package com.gitee.dfdiot.sdk.entity;

import java.io.Serializable;

/**
 * 时序数据库相关参数
 */
public class DfdTdDataParams implements Serializable {
    private String deviceId;
    private String moduleMark;
    private String endTime;
    private String startTime;
    private Integer dataNum;

    private DfdTdDataParams() {
    }

    public static DfdTdDataParams getDataList(String deviceId, String moduleMark, Integer dataNum, String startTime, String endTime) {
        DfdTdDataParams dfdTdDataParams = new DfdTdDataParams();
        dfdTdDataParams.setDeviceId(deviceId);
        dfdTdDataParams.setModuleMark(moduleMark);
        dfdTdDataParams.setDataNum(dataNum);
        dfdTdDataParams.setStartTime(startTime);
        dfdTdDataParams.setEndTime(endTime);
        return dfdTdDataParams;
    }

    private void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    private void setModuleMark(String moduleMark) {
        this.moduleMark = moduleMark;
    }

    private void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    private void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    private void setDataNum(Integer dataNum) {
        this.dataNum = dataNum;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getModuleMark() {
        return moduleMark;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public Integer getDataNum() {
        return dataNum;
    }
}
