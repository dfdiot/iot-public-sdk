package com.gitee.dfdiot.sdk.config;

/**
 * @author HuYong
 * @date 2022/9/6
 */
public enum  ListenerStatus {

    LISTENER_SUCCESS("0","LISTENER_SUCCESS"),
    LISTENER_FAIL("500","LISTENER_FAIL");

    private String code;
    private String message;

    ListenerStatus(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
