package com.gitee.dfdiot.sdk.listener;

/**
 * @author HuYong
 * @date 2022/9/6
 */
public interface RocketListenerCallback {

    /**
     * 监听正常数据
     */
    void onSuccess(RocketListenerResult var1);

    /**
     * 监听异常
     * @param var1
     */
    void onException(Throwable var1);

}
