package com.gitee.dfdiot.sdk.config;

import java.io.Serializable;

public class DfdConfig implements Serializable {
    private String apiVersion = "v1";
    private String signatureMethod = "HMAC-SHA1";
    private String openUrl = "https://iotdev.dfdiot.com/iot/openapi/";
    private String accessSecret = "yourAccessSecret";
    private String accessKey = "yourAccessKey";
    private long timestamp = System.currentTimeMillis();

    private DfdConfig() {
    }

    public DfdConfig(String accessKey, String accessSecret) {
        this.accessSecret = accessSecret;
        this.accessKey = accessKey;
    }

    public DfdConfig(String accessKey, String accessSecret, String apiVersion) {
        this.accessSecret = accessSecret;
        this.accessKey = accessKey;
        this.apiVersion = apiVersion;
    }

    public DfdConfig(String apiVersion, String signatureMethod, String accessSecret, String accessKey, long timestamp) {
        this.apiVersion = apiVersion;
        this.signatureMethod = signatureMethod;
        this.accessSecret = accessSecret;
        this.accessKey = accessKey;
        this.timestamp = timestamp;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getAccessSecret() {
        return accessSecret;
    }

    public void setAccessKeySecret(String accessSecret) {
        this.accessSecret = accessSecret;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getSignatureMethod() {
        return signatureMethod;
    }

    public void setSignatureMethod(String signatureMethod) {
        this.signatureMethod = signatureMethod;
    }

    public String getOpenUrl() {
        return openUrl;
    }

    public void setOpenUrl(String openUrl) {
        this.openUrl = openUrl;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
