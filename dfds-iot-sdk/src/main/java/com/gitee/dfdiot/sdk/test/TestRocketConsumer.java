package com.gitee.dfdiot.sdk.test;

import com.gitee.dfdiot.sdk.config.RocketMqClientConfig;
import com.gitee.dfdiot.sdk.listener.RocketMqPushConsumer;

/**
 * @author HuYong
 * @date 2022/9/6
 */
public class TestRocketConsumer {

    public static void main(String[] args) {
        //设置消费者必要信息
        RocketMqClientConfig clientConfig = new RocketMqClientConfig();
        clientConfig.setAccessKey("fnaozuMELcVOhiEh");
        clientConfig.setSecretKey("hMOcDkvCcYJbR7n8SDTm8gzk3BLhkBUS");
        clientConfig.setConsumerGroupId("XtAfCGoTbXYKXLsY");

        //创建消费者对象
        RocketMqPushConsumer pushConsumer = new RocketMqPushConsumer(clientConfig);
        pushConsumer.start(new RocketListenerCallbackTest());

        System.out.println("启动消费者，开始监听....");
    }
}
