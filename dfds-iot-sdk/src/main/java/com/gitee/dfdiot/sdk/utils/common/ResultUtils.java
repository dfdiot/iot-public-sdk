package com.gitee.dfdiot.sdk.utils.common;

import com.alibaba.fastjson.JSONObject;


public class ResultUtils {

    public static JSONObject returnJson(String resqonse) {
        JSONObject result = new JSONObject();
        result.put("code", -1);
        result.put("message", "请求失败");
        try {
            if ("fail".equals(resqonse) || null == resqonse) {
                return result;
            } else {
                return JSONObject.parseObject(resqonse);
            }
        } catch (Exception e) {
            return result;
        }
    }
}
