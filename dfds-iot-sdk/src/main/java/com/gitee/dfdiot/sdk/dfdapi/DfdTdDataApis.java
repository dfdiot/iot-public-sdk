package com.gitee.dfdiot.sdk.dfdapi;

import com.alibaba.fastjson.JSONObject;
import com.gitee.dfdiot.sdk.config.DfdConfig;
import com.gitee.dfdiot.sdk.entity.DfdTdDataParams;
import com.gitee.dfdiot.sdk.service.DfdTdDataService;

/**
 * 数据管理_相关接口
 * yuanjin
 * 2022-10-11
 */
public class DfdTdDataApis {
    private DfdConfig dfdConfig;

    public DfdTdDataApis(DfdConfig dfdConfig) {
        this.dfdConfig = dfdConfig;
    }

    private DfdTdDataService dfdTdDataService = DfdTdDataService.getInstance();

    public JSONObject getDataList(DfdTdDataParams page) {
        return dfdTdDataService.getDataList(page, dfdConfig);
    }

}
