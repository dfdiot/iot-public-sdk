package com.gitee.dfdiot.sdk.service;

import com.alibaba.fastjson.JSONObject;
import com.gitee.dfdiot.sdk.config.DfdConfig;
import com.gitee.dfdiot.sdk.entity.DfdTdDataParams;
import com.gitee.dfdiot.sdk.utils.common.ParamsUtils;
import com.gitee.dfdiot.sdk.utils.common.ResultUtils;
import com.gitee.dfdiot.sdk.utils.request.HttpClientUtil;
import com.gitee.dfdiot.sdk.utils.signs.SignUtil;

import java.util.Map;

/**
 * 时序数据库相关
 * yuanjin
 * 2022-10-14
 */
public class DfdTdDataService {
    private final String tddata = "/tddata";
    private HttpClientUtil instance = HttpClientUtil.getInstance();
    private static DfdTdDataService dfdDeviceService = null;

    private DfdTdDataService() {
    }

    public static DfdTdDataService getInstance() {
        if (dfdDeviceService == null) {
            dfdDeviceService = new DfdTdDataService();
        }
        return dfdDeviceService;
    }

    /**
     * 查询数据查询信息
     * params {
     * "dataNum": 10（下发指令操作类型）
     * "endTime":1691273181321
     * "startTime":1691273185321
     * }
     */
    public JSONObject getDataList(DfdTdDataParams page, DfdConfig dfdConfig) {
        try {
            Map<String, String> getParams = SignUtil.generate("POST", ParamsUtils.getAllParamNoOther(dfdConfig, page), dfdConfig.getAccessSecret());
            String response = instance.doPost(dfdConfig.getOpenUrl() + dfdConfig.getApiVersion()  + tddata + "/getDataList", getParams);
            return ResultUtils.returnJson(response);
        } catch (Exception e) {
        }
        return ResultUtils.returnJson(null);
    }

}

