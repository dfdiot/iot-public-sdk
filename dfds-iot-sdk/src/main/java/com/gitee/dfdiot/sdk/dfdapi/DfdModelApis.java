package com.gitee.dfdiot.sdk.dfdapi;

import com.alibaba.fastjson.JSONObject;
import com.gitee.dfdiot.sdk.config.DfdConfig;
import com.gitee.dfdiot.sdk.entity.DfdModelParams;
import com.gitee.dfdiot.sdk.service.DfdModelService;


/**
 * 模型管理_相关接口
 * yuanjin
 * 2022-10-11
 */
public class DfdModelApis {
    private DfdConfig dfdConfig;

    public DfdModelApis(DfdConfig dfdConfig) {
        this.dfdConfig = dfdConfig;
    }

    private DfdModelService dfdModelService = DfdModelService.getInstance();

    public JSONObject getModelInfo(DfdModelParams modelParams) {
        return dfdModelService.getModelInfo(modelParams, dfdConfig);
    }
}
