package com.gitee.dfdiot.sdk.config;

/**
 * RocketMQ客户端配置
 * @date 2022/8/19
 */
public class RocketMqClientConfig {
    
    /**
     * RocketMq服务地址
     * 注意：不允许用户更改
     */
    private String nameServerAddr = "nsrv01.dfdiot.com:9876;nsrv02.dfdiot.com:9876";

    /**
     * 消费组ID
     */
    private String consumerGroupId;

    /**
     * 用户accessKey
     * 在东方合智IOT平台_访问控制 获取
     */
    private String accessKey;

    /**
     * 用户secretKey
     * 在东方合智IOT平台_访问控制 获取
     */
    private String secretKey;

    /**
     * 消费线程数
     * 注意：用户可根据实际环境的服务器资源进行配置
     */
    private int consumerThreadNum = 0;


    public String getNameServerAddr() {
        return nameServerAddr;
    }

    public String getConsumerGroupId() {
        return consumerGroupId;
    }

    public void setConsumerGroupId(String consumerGroupId) {
        this.consumerGroupId = consumerGroupId;
    }

    public String getSubscribeTopic() {
        return getConsumerGroupId();
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public int getConsumerThreadNum() {
        return consumerThreadNum;
    }

    public void setConsumerThreadNum(int consumerThreadNum) {
        this.consumerThreadNum = consumerThreadNum;
    }

}
