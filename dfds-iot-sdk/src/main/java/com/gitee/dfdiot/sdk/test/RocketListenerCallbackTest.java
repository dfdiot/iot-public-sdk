package com.gitee.dfdiot.sdk.test;

import com.gitee.dfdiot.sdk.listener.RocketListenerResult;
import com.gitee.dfdiot.sdk.listener.RocketListenerCallback;

/**
 * @author HuYong
 * @date 2022/9/6
 */
public class RocketListenerCallbackTest implements RocketListenerCallback {

    @Override
    public void onSuccess(RocketListenerResult var1) {
        System.out.println("接收到消息，开始处理自己业务："+var1.getListenerStatus() + "-----" + var1.getMessageBody());
        //用户在这里处理自己的业务流程

    }

    @Override
    public void onException(Throwable var1) {
        throw new RuntimeException("RocketMq Consumer Listener onException",var1);
    }
}
