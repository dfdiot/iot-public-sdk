package com.gitee.dfdiot.sdk.listener;

import com.gitee.dfdiot.sdk.config.ListenerStatus;

/**
 * @author HuYong
 * @date 2022/9/6
 */
public class RocketListenerResult {

    private ListenerStatus listenerStatus;

    private String messageBody;

    public ListenerStatus getListenerStatus() {
        return listenerStatus;
    }

    public void setListenerStatus(ListenerStatus listenerStatus) {
        this.listenerStatus = listenerStatus;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}
