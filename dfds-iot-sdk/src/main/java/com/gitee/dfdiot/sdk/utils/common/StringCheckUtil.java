package com.gitee.dfdiot.sdk.utils.common;

import org.apache.commons.lang3.StringUtils;


/**
 * @author HuYong
 * @date 2022/1/7 16:22
 */
public class StringCheckUtil {

    /**
     * 检查字符串是否为空 (支持传入多个需校验非空参数)
     * @param value
     * @return false:不为空 true:为空
     */
    public static boolean isEmpty(String... value){
        int count = 0;
        for(int i=0; i<value.length; i++){
            //遍历字符串数组里面的所有参数，发现某个参数为 null或者""，则跳出
            if(StringUtils.isEmpty(value[i])){
                break;
            }
            count++;
        }
        if(count == value.length){
            return false;
        }
        return true;
    }
}
